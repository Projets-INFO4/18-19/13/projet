# Projet

## Requirements

* Retreive the NordicID API : https://github.com/NordicID/nur_sample_java , clone the git in a folder of your choice 
 

* When you're done downloading the API, open the eclispe project, Rigth click on the project > propreties > Java build path > Libraries, add the follow _.jar_:

* * __NurApi.jar__ : your\_Api\_folder/Nur_Api/java

* * __NurApiSerialTransport.jar__ : your\_workspace/Nur/nur\_sample\_java/transports/jars

* * __NurApiSocketTransport.jar__ : your\_workspace/Nur/nur\_sample\_java/transports/jars

* * __RXTX.jar__ : /usr/share/java

<br></br>

* Rigth click on the project > run as > run configuration > Main > Arguments , add into VM arguments field the line : __-Djava.library.path=/usr/lib/jni__
 
