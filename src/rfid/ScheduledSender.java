package rfid;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

/**
 * the aim of this class is to send information about the passage of a runner
 * near the crossing point
 *
 * 
 * @author ubuntu
 *
 */
public class ScheduledSender implements ISender {

	/**
	 * This Queue is FIFO and will contain the data to send to the LoRa antenna.
	 * 
	 */
	private Queue<Passage> m_storage;

	/**
	 * To ensure that no thread will try to remove the first elements of the Queue.
	 * 
	 */
	private Semaphore m_sem;

	/**
	 * 
	 */
	private Thread m_scheduler;

	/**
	 * 
	 */
	public ScheduledSender() {
		init();
	}

	private void init() {
		m_storage = new LinkedList<Passage>();
		m_sem = new Semaphore(0);
		m_scheduler = new Scheduler();
		m_scheduler.start();
	}

	/**
	 * 
	 * @param p
	 * 
	 * 
	 */
	public void addPassage(Passage p) {
		m_storage.add(p);
		m_sem.release();
	}

	public void sendPassage() {
		try {
			m_sem.acquire();
			Passage p = m_storage.remove();
			// TODO Send p via LoRaWan
			Calendar c = new GregorianCalendar();
			c.setTimeInMillis(p.m_time);
			
			String date = c.getTime().toString();
			
			System.out.println("coureur n°" + p.getTag().getEpcString() + " à passé ce point de passage à: "+date);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @author ubuntu
	 */
	private class Scheduler extends Thread {

		@Override
		public void run() {
			// TODO Call the Js method that will
			while (true) {
				sendPassage();
			}
		}
	}
}
