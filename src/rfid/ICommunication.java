package rfid;

import java.io.IOException;

public interface ICommunication {
	
	public void send(Passage p) throws IOException;
	
	public void start() throws IOException;
	
	public void close() throws IOException;
	
	public int getStatus();

}
