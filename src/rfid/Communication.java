package rfid;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Communication implements ICommunication {
	private static final int BUFFER_SIZE = 256;
	private static final int DEFAULT_PORT = 8080;

	public static final int STATUS_STARTED = 1;
	public static final int STATUS_CONNEXION_ERROR = 3;
	public static final int STATUS_IDLE = 2;

	private int m_port;
	private byte m_buffer[];
	private int m_status = STATUS_IDLE;
	private ServerSocket m_socket;
	private Socket m_client;
	private OutputStream m_os;
	private DataOutputStream m_dos;

	public Communication(int port) {
		m_buffer = new byte[BUFFER_SIZE];
		m_port = port;
	}

	public Communication() {
		m_buffer = new byte[BUFFER_SIZE];
		m_port = DEFAULT_PORT;
	}

	@Override
	public void send(Passage p) throws IOException{
		// TODO Auto-generated method stub
		if (m_status == STATUS_STARTED) {

		}
	}

	@Override
	public void start() throws IOException{
		try {
			m_socket = new ServerSocket(m_port);

			Socket m_client = m_socket.accept();
			System.out.println("Client " + m_client.getInetAddress() + " connected.");

			m_os = m_client.getOutputStream();
			m_dos = new DataOutputStream(m_os);

			m_status = STATUS_STARTED;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			m_status = STATUS_CONNEXION_ERROR;
			throw e;
		}

	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		if (m_status == STATUS_STARTED) {
			m_socket.close();
			m_status = STATUS_IDLE;
		}

	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return m_status;
	}

}
