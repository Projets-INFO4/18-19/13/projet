package rfid;
/**
 * Interface of a Sender wich have to send datas via LoRaWan
 * 
 * @author ubuntu
 *
 */
public interface ISender {
	
	public void addPassage(Passage p) ;
	public void sendPassage()  ;
}
