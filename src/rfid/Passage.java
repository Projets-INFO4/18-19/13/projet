package rfid;

import java.io.Serializable;

import com.nordicid.nurapi.NurTag;

public class Passage implements Serializable{
	public long m_time;		// the passage time
	private NurTag m_tag;	// the competitor's number
//	private int m_device_id; not sure if it's needed from a practical point of view
	
	public Passage(long time,NurTag tag) {
		m_time = time;
		m_tag = tag;
	}
	
	public NurTag getTag() {
		return m_tag;
	}
}
