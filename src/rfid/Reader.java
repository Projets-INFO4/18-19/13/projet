package rfid;

import com.nordicid.nurapi.NurApi;
import com.nordicid.nurapi.NurApiListener;
import com.nordicid.nurapi.NurEventAutotune;
import com.nordicid.nurapi.NurEventClientInfo;
import com.nordicid.nurapi.NurEventDeviceInfo;
import com.nordicid.nurapi.NurEventEpcEnum;
import com.nordicid.nurapi.NurEventFrequencyHop;
import com.nordicid.nurapi.NurEventIOChange;
import com.nordicid.nurapi.NurEventInventory;
import com.nordicid.nurapi.NurEventNxpAlarm;
import com.nordicid.nurapi.NurEventProgrammingProgress;
import com.nordicid.nurapi.NurEventTagTrackingChange;
import com.nordicid.nurapi.NurEventTagTrackingData;
import com.nordicid.nurapi.NurEventTraceTag;
import com.nordicid.nurapi.NurEventTriggeredRead;
import com.nordicid.nurapi.NurTag;
import com.nordicid.nurapi.NurTagStorage;

public class Reader {

	private NurApi m_nurapi;
	private ISender m_sender;
	private long m_starting_time;

	public Reader() throws Exception {
		m_sender = new ScheduledSender();
		m_nurapi = SamplesCommon.createAndConnectNurApi();
		m_nurapi.setListener(new ReaderBehavior());
	}

	public void clear() throws Exception {
		try {
			m_nurapi.clearIdBuffer(true);
			System.out.println("Clearing the tag storage!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			String message = "An exception occured!: " + "class Reader --> public void clear();";
			System.out.println(message);
			throw e;
		}

	}

	public void start() throws Exception {
		System.out.println("Start inventory stream");
		m_nurapi.startInventoryStream();
		m_starting_time = System.currentTimeMillis();
	}

	private class ReaderBehavior implements NurApiListener {

		@Override
		public void IOChangeEvent(NurEventIOChange arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void autotuneEvent(NurEventAutotune arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void bootEvent(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void clientConnectedEvent(NurEventClientInfo arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void clientDisconnectedEvent(NurEventClientInfo arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void connectedEvent() {
			// TODO Auto-generated method stub

		}

		@Override
		public void debugMessageEvent(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void deviceSearchEvent(NurEventDeviceInfo arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void disconnectedEvent() {
			// TODO Auto-generated method stub

		}

		@Override
		public void epcEnumEvent(NurEventEpcEnum arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void frequencyHopEvent(NurEventFrequencyHop arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void inventoryExtendedStreamEvent(NurEventInventory arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void inventoryStreamEvent(NurEventInventory arg0) {
			// TODO Auto-generated method stub

			NurTagStorage apiStorage = m_nurapi.getStorage();

			// When accessing api owned storage from event, we need to lock it
			synchronized (apiStorage) {
				// Add inventoried tags to our unique tag storage

				long real_time; // to store the real time which when we detected the tag;
				for (int n = 0; n < apiStorage.size(); n++) {
					NurTag tag = apiStorage.get(n);
					
					
					real_time = System.currentTimeMillis();

					m_sender.addPassage(new Passage(real_time, tag));
					apiStorage.clear();
				}
			}
			
			if (arg0.stopped) {
				try {
					start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void logEvent(int arg0, String arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void nxpEasAlarmEvent(NurEventNxpAlarm arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void programmingProgressEvent(NurEventProgrammingProgress arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void tagTrackingChangeEvent(NurEventTagTrackingChange arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void tagTrackingScanEvent(NurEventTagTrackingData arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void traceTagEvent(NurEventTraceTag arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void triggeredReadEvent(NurEventTriggeredRead arg0) {
			// TODO Auto-generated method stub

		}

	}
}
